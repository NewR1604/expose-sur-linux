# expose-sur-linux

Ceci est un exposé sur Linux expliquant plusieurs choses sur lui comme : l'histoire, la licence, les avantages à l'utiliser, ... etc.

# Instructions

Pour visionner le contenu entier de l'exposé, ouvrez "index.html", sinon, si vous voulez juste visionner la synthèse, ouvrez "expose-sur-linux-synthese.odp".

# Licence

Cet exposé est publié sous le GNU GPLv3 ce qui veut dire que vous pouvez faire ce que vous voulez de cet exposé à condition qu'il soit partagé sous les mêmes conditions.

# Codeurs

Le codeur original de cet exposé est NewR1604 (Ryan Justin Palmer).

# Contributeurs

Voici les noms de tout les contributeurs :

# Contact

Voici divers moyens de me contacter :
-par mail : ryanjustinpalmer@gmail.com
-par SMS ou par téléphone : dans ce cas vous devez avoir mon numéro de téléphone que je vous donnerai si c'est nécéssaire
-notez que je vous bloquerai si vous me spamez.
